victories1 = 0
victories2 = 0

while victories1 < 3 and victories2 < 3:
    player1 = input(" Jugador 1 >>>>> Piedra, Papel, Tijera, Lagarto, Spock:").lower()
    player2 = input(" Jugador 2 >>>>> Piedra, Papel, Tijera, Lagarto, Spock:").lower()
    if player1 == "piedra" and player2 == "tijera":
        victories1 += 1
    elif player1 == "piedra" and player2 == "lagarto":
        victories1 += 1
    elif player1 == "papel" and player2 == "piedra":
        victories1 += 1
    elif player1 == "papel" and player2 == "spock":
        victories1 += 1
    elif player1 == "tijera" and player2 == "papel":
        victories1 += 1
    elif player1 == "tijera" and player2 == "lagarto":
        victories1 += 1
    elif player1 == "lagarto" and player2 == "papel":
        victories1 += 1
    elif player1 == "lagarto" and player2 == "spock":
        victories1 += 1
    elif player1 == "spock" and player2 == "tijera":
        victories1 += 1
    elif player1 == "spock" and player2 == "piedra":
        victories1 += 1
    else:
        victories2 += 1

if victories1 > victories2:
    print("Player 1 es el ganador!")
else:
    print("Player 2 es el ganador!")