def cesarCode():
    
    alphabet = ("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N",
                "�", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "A",
                "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O",
                "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z")

    jump = int(input("Ingrese el n�mero de saltos del c�digo cesar: "))
    phrase = input("Ingrese la palabra u oraci�n a codificar: ").upper()
    code = ""

    for k in phrase:
        for j in range(0, 27):
            if k == alphabet[j]:
                code += alphabet[j + jump]

    return code