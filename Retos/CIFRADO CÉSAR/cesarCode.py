alphabet = ("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N",
            "Ñ", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "A",
            "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O",
            "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z")

# Se asigna la variable code como tipo string pero vac�a
jump = int(input("Ingrese el n�mero de saltos del c�digo cesar: "))
phrase = input("Ingrese la palabra u oraci�n a codificar: ").upper()
code = ""

# El primer ciclo es para recorrer la frase a codificar. El segundo ciclo es para recorrer el alfabeto y subordinado a este
# segundo ciclo un condicional que compara el valor de la frase con el del alfabeto y cuando sean iguales se guardar� en la
# variable code con su valor codificado.
for k in phrase:
    for j in range(0, 27):
        if k == alphabet[j]:
            code += alphabet[j + jump]

print("Texto codificado: \n", code)