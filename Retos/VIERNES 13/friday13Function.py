from calendar import monthcalendar

def friday13(year, month):
    
    date = monthcalendar(year, month)

    if date[1][4] == 13 or date[2][4] == 13:
        print(True)
    else:
        print(False)
            
    return