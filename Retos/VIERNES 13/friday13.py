from calendar import monthcalendar

month = int(input("Ingrese el mes (ingrese el número del mes): "))
year = int(input("Ingrese el año: "))

date = monthcalendar(year, month)

if date[1][4] == 13 or date[2][4] == 13:
    print(True)
else:
    print(False)