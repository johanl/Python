import random

words = ["Software", "Hardware", "Python", "Zotero", "Firefox", "Inkscape", "Scribus", "LibreOffice", "Cookies", "Ceferino",
         "Supertux", "Gparted", "ThunderBird", "Colobot", "Spyder", "Emacs", "Memoria", "RStudio", "Octave", "NextCloud", "Gitea"]

# word será una lista y tendrá como valor de la lista words, este valor se elegirá al azar y se pondrá en mayúsculas
word = list(words[random.randint(0, len(words) - 1)].upper())
hang = word.copy()
k = intents = 0

# Se necesitó copiar la lista word y a la copia se le cambian los valores por "__"
while k < len(hang):
    hang[k] = "__"
    k += 1

# Se pondrá una letra de la lista word en la lista hang, este proceso es aleatorio
for j in range(0, len(word), 2):
    index = random.randint(0, len(word) - 1)
    hang[index] = word[index]

print("\n === Adivina la palabra ===\n\n\n", hang)

while intents < 5:
    letter = input("Ingrese una letra: ").upper()
    index2 = int(input("Ingrese el lugar donde pondrá la letra: "))
    if letter != word[index2 - 1]:
        print("No ha acertado\n\n === Adivina la palabra ===\n\n\n", hang)
        intents += 1
    else:
        hang[index2 - 1] = letter
        print("\n === Adivina la palabra ===\n\n\n", hang)
        
print(hang)