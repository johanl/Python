from random import randint

def riddle(words: list, intent: int):

    word = list(words[randint(0, len(words))].upper())
    hang = word.copy()
    k = 0
    intents = 0

    while k < len(hang):
        hang[k] = "__"
        k += 1

    for j in range(0, len(word), 2):
        index = randint(0, len(word) - 1)
        hang[index] = word[index]

    print("\n === Adivina la palabra ===\n\n\n", hang)

    while intents < intent:
        letter = input("Ingrese una letra: ").upper()
        index2 = int(input("Ingrese el lugar donde pondrá la letra: "))
        if letter != word[index2 - 1]:
            print("No ha acertado\n\n === Adivina la palabra ===\n\n\n", hang)
            intents += 1
        else:
            hang[index2 - 1] = letter
            print("\n === Adivina la palabra ===\n\n\n", hang)
        
    return hang