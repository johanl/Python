# BlackJack

import random

deck = {"A":11, "King":10, "Queen":10, "Jack":10, 10:10, 9:9, 8:8, 7:7, 6:6,
        5:5, 4:4, 3:3, 2:2, "Ace":1}

game = []

for k in range(0, 2):
        game.append(deck.get(random.choice(list(deck.keys()))))
        
print("Mano sin tomar otra carta ", sum(game))

while sum(game) <= 21:
    hand = input("¿Desea otra carta?\n [y/n]").lower()
    if hand == 'y':
        game.append(deck.get(random.choice(list(deck.keys()))))
        print("Su mano es ", sum(game))
    else:
        break
        
print("Su Mano actual es ", sum(game))