def primeNumber(number):
    counter = 0
    for k in range(2, number):
        if number % k == 0:
            counter += 1
    
    if counter == 0:
        print("El número ", number, " es primo.")
    else:
        print("El número ", number, " no es primo.")
        
    return