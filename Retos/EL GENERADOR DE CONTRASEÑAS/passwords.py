import random

capital = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N",
            "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
    
numbers = ["1", "2","3", "4", "5", "6", "7", "8", "9", "0"]

lowercase = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n",
            "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]

symbols = ["|", "°", "¬", "!", "@", "#", "·", "$", "%", "&", "/", "(", ")", "=",
          "?", "'", "¡", "¿", "+", "*", "~", "{", "}", "]", "[", "-", "_", ".",
          ":", ";", "<", ">"]

password = ""

long = int(input("Longitud de la contraseña, 8 o 16: "))
elements1 = int(input("¿Mayúsculas? - 1 para si, 0 para no: "))
elements2 = int(input("¿Minúsculas? - 1 para si, 0 para no: "))
elements3 = int(input("¿Símbolos? - 1 para si, 0 para no: "))
elements4 = int(input("¿Números? - 1 para si, 0 para no: "))

while len(password) < long:
    if elements1 == 1:
        password += random.choice(capital)
    if elements2 == 1:
        password += random.choice(lowercase)
    if elements3 == 1:
        password += random.choice(symbols)
    if elements4 == 1:
        password += random.choice(numbers)

print(password)