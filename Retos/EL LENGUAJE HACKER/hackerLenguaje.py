leet = {"A": "4", "B": "I3", "C": "[", "D": ")", "E": "3", "F": "|=", "G": "&", "H": "#", "I": "1",
        "J": ",_|", "K": ">|", "L": "1", "M": "/\/\\", "N": " ^/", "O": "0", "P": " |*", "Q": "(_,)",
        "R": "I2", "S": "5", "T": "7", "U": "(_)", "V": "\/", "W": "\/\/", "X": "><", "Y": "j", "Z": "2",
        "1": "L", "2": "R", "3": "E", "4": "A", "5": "S", "6": "b", "7": "T", "8": "B", "9": "g", "0": "o"}

# Se declara emptyworld como string pero vacía para ir agregando las letras de la traducción
word = input("Ingrese una palabra que desee traducir: ")
emptyWord = ""

# Compara la letra de la palabra a traducir con la llave del diccionario definida por las letras del alfabeto. Y aquella que
# concuerde se asignará el valor de la respectiva llave en la variable emptyWord.
for k in word:
    if k.upper() in leet.keys():
        emptyWord += leet[k.upper()]
    else:
        emptyWord += k
        
print("\n La traducción es: ", emptyWord)